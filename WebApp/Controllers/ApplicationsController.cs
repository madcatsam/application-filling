﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApp.Models;
using WebApp.Models.Dto;

namespace WebApp.Controllers
{
    [RoutePrefix("api/applications")]
    public class ApplicationsController : ApiController
    {

        // TODO: Make async/awit controller's operations

        [Route("")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            using (var session = NHibernateSession.GetSession())
            {
                var applications = session.CreateCriteria<Application>().List<Application>();

                var applicationDtos = applications.Select(application => new ApplicationDto // TODO: in order to avoid such code, use AutoMapper or something else
                    {
                        Id = application.Id,
                        Date = application.Date,
                        OrganizationName = application.OrganizationName,
                        UserFullName = application.UserFullName,
                        Position = application.Position,
                        Email = application.Email
                    })
                    .ToList();

                return Ok(applicationDtos);
            }
        }

        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            using (var session = NHibernateSession.GetSession())
            {
                var application = session.Get<Application>(id);

                if (application == null)
                    return NotFound();

                return Ok(new ApplicationDto
                {
                    Id = application.Id,
                    Date = application.Date,
                    OrganizationName = application.OrganizationName,
                    UserFullName = application.UserFullName,
                    Position = application.Position,
                    Email = application.Email
                });
            }
        }

        [Route("{id}")]
        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody] ApplicationDto applicationDto)
        {
            if (!IsDtoValid(applicationDto))
                return BadRequest();

            using (var session = NHibernateSession.GetSession())
            using (var tx = session.BeginTransaction())
            {
                var application = new Application
                {
                    Id = applicationDto.Id,
                    Date = applicationDto.Date,
                    OrganizationName = applicationDto.OrganizationName,
                    UserFullName = applicationDto.UserFullName,
                    Position = applicationDto.Position,
                    Email = applicationDto.Email
                };

                session.Update(application);
                tx.Commit();

                return Ok();
            }
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] ApplicationDto applicationDto)
        {
            if (!IsDtoValid(applicationDto))
                return BadRequest();

            using (var session = NHibernateSession.GetSession())
            using (var tx = session.BeginTransaction())
            {
                var application = new Application
                { // No need to pass Id as it's autoincremented field
                    Date = applicationDto.Date,
                    OrganizationName = applicationDto.OrganizationName,
                    UserFullName = applicationDto.UserFullName,
                    Position = applicationDto.Position,
                    Email = applicationDto.Email
                };

                session.Save(application);
                tx.Commit();

                return Created(new Uri(Request.RequestUri + "/" + application.Id), applicationDto);
            }
        }

        [Route("{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            
            using (var session = NHibernateSession.GetSession())
            using (var tx = session.BeginTransaction())
            {
                var application = session.Load<Application>(id);
                if (application == null)
                    return NotFound();

                session.Delete(application);
                tx.Commit();

                return Ok();
            }
        }

        private bool IsDtoValid(ApplicationDto applicationDto)
        {
            if (string.IsNullOrEmpty(applicationDto.OrganizationName))
                return false;
            if (string.IsNullOrEmpty(applicationDto.UserFullName))
                return false;
            if (string.IsNullOrEmpty(applicationDto.Position))
                return false;
            if (string.IsNullOrEmpty(applicationDto.Email))
                return false;

            return true;
        }
    }
}
