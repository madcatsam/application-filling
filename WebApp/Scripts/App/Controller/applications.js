﻿Ext.define('App.controller.applications', {
    extend: 'Ext.app.Controller',
    stores: [
        'App.model.applicationStore'
    ],
    views: [
        'App.view.applications'
    ],
    refs: [
        {
            ref: 'addButton',
            selector: 'button#addApplicationButton'
        },
        {
            ref: 'refreshButton',
            selector: 'button#refreshApplicationButton'
        }
    ],
    requires: [
        'App.controller.applicationForm'
    ],
    grid: undefined,
    viewport: undefined,
    init: function (app) {
        console.log('applicationsController init');

        this.control({
            '#applicationGrid actioncolumn': {
                'APPLICATION_EDITED': function (e) {

                    var controller = this.application.getController('App.controller.applicationForm');

                    var date = Ext.Date.parse(e.raw.Date, 'Y-d-mTH:i:s');
                    console.log(e.raw);
                    controller.show({
                        Id: e.raw.Id,
                        Date: Ext.Date.format(date, 'd.m.Y'),
                        OrganizationName: e.raw.OrganizationName,
                        UserFullName: e.raw.UserFullName,
                        Position: e.raw.Position,
                        Email: e.raw.Email
                    });

                    this.application.on('FORM_CONTROLLER_APPLY', this.onFormControllerAddOrUpdate, this);
                    this.application.on('FORM_CONTROLLER_CLOSED', this.onFormControllerClosed, this);
                }
            }
        });
    },
    renderTo: function (viewport) {
        var store = this.getStore('App.model.applicationStore');
        var gr = this.getView('App.view.applications');
        this.grid = Ext.create(gr);
        this.grid.bindStore(store);
        this.viewport = viewport;
        this.addComponent(this.grid);
        this.getAddButton().on('click', this.add, this);
        this.getRefreshButton().on('click', this.refresh, this);
    },
    addComponent: function (component) {
        this.viewport.add(component);
    },
    add: function (button) {
        var controller = this.application.getController('App.controller.applicationForm');
        controller.show();

        this.application.on('FORM_CONTROLLER_APPLY', this.onFormControllerAddOrUpdate, this);
        this.application.on('FORM_CONTROLLER_CLOSED', this.onFormControllerClosed, this);
    },
    refresh: function (button) {
        var store = this.getStore('App.model.applicationStore');
        store.load();
    },
    onFormControllerAddOrUpdate: function (values) {
        var store = this.getStore('App.model.applicationStore');

        console.log(values);

        if (values.Id === undefined || values.Id === null || +values.Id === 0) { // Create new entry

            var newApplication = Ext.create('App.model.application', {
                Id: 0,
                Date: Ext.Date.parse(values.Date, 'd.m.Y'),
                OrganizationName: values.OrganizationName,
                UserFullName: values.UserFullName,
                Position: values.Position,
                Email: values.Email
            });

            newApplication.phantom = true;
            store.add(newApplication);
        } else { // Update existing one

            var existingApplication = store.findRecord('Id', values.Id);
            existingApplication.set('Date', Ext.Date.parse(values.Date, 'd.m.Y'));
            existingApplication.set('OrganizationName', values.OrganizationName);
            existingApplication.set('UserFullName', values.UserFullName);
            existingApplication.set('Position', values.Position);
            existingApplication.set('Email', values.Email);
            existingApplication.commit();

            existingApplication.dirty = true;
        }

        store.sync({
            failure: function (data) {
                store.rejectChanges();
                store.load();
                alert('Some error occred'); // TODO: Make more informative
            },
            success: function (data) {
                store.load();
            }
        });
    },
    onFormControllerClosed: function () {
        this.application.un('FORM_CONTROLLER_APPLY', this.onFormControllerAddOrUpdate, this);
        this.application.un('FORM_CONTROLLER_CLOSED', this.onFormControllerClosed, this);
    }
});
