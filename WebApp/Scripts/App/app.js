﻿Ext.require([
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.util.*',
    'Ext.state.*'
]);

Ext.Loader.setPath('App', 'Scripts/app');
Ext.Loader.setConfig({
    enabled: true,
    path: { 'App': 'Scripts/app' }
});

Ext.onReady(function () {
    Ext.QuickTips.init();
    Ext.application({
        appFolder: 'Scripts/app',
        name: 'App',
        autoCreateViewport: true,
        requires: ['App.view.Viewport'],
        controllers: ['App.controller.applications'],
        stores: ['App.model.application'],

        launch: function () {
            var viewPort = Ext.ComponentQuery.query('viewport')[0];

            var controller = this.getController('App.controller.applications');
            controller.renderTo(viewPort);
        }
    });
});


