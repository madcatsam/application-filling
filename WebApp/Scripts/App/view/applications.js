﻿Ext.define('App.view.applications', {
    extend: 'Ext.grid.Panel',
    title: 'Application list',
    itemId: 'applicationGrid',
    columns: [
        {
            text: 'Id',
            width: 20,
            sortable: true,
            hideable: false,
            dataIndex: 'Id'
        },
        {
            text: 'Date',
            width: 100,
            sortable: true,
            hideable: false,
            dataIndex: 'Date',
            editor: {
                xtype: 'datecolumn',
                allowBlank: false
            }
        },
        {
            text: 'OrganizationName',
            width: 100,
            sortable: true,
            hideable: false,
            dataIndex: 'OrganizationName'
        },
        {
            text: 'UserFullName',
            width: 100,
            sortable: true,
            hideable: false,
            dataIndex: 'UserFullName'
        },
        {
            text: 'Position',
            width: 100,
            sortable: true,
            hideable: false,
            dataIndex: 'Position'
        },
        {
            text: 'Email',
            width: 100,
            sortable: true,
            hideable: false,
            dataIndex: 'Email'
        },
        {
            xtype: 'actioncolumn',
            width: 50,
            items: [
                {
                    icon: 'Images/edit.png', 
                    tooltip: 'Edit',
                    handler: function (grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.fireEvent('APPLICATION_EDITED', rec);
                    }
                }, {
                    icon: 'Images/delete.png',
                    tooltip: 'Delete',
                    handler: function (grid, rowIndex, colIndex) {
                        Ext.MessageBox.confirm('Confirm', "Remove entry?", function (btn, text) {
                            if (btn == 'yes') {
                                var rec = grid.getStore().getAt(rowIndex);
                                grid.getStore().remove(rec);
                                grid.getStore().sync();
                            }
                        });

                    }
                }
            ]
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    xtype: 'button',
                    name: 'add',
                    text: 'Create application',
                    itemId: 'addApplicationButton'
                },
                {
                    xtype: 'button',
                    name: 'refresh',
                    text: 'Refresh list',
                    itemId: 'refreshApplicationButton'
                }
            ]
        },
        {
            xtype: 'pagingtoolbar',
            store: Ext.data.StoreManager.get("App.model.applicationStore"),
            dock: 'bottom',
            displayInfo: true,
            beforePageText: 'Page',
            afterPageText: 'of {0}',
            displayMsg: '{0} - {1} of {2}'
        }]
});
