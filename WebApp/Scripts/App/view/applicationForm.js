﻿Ext.define('App.view.applicationForm', {
    extend: 'Ext.window.Window',
    id: 'applicationWindow',
    border: 0,
    height: 350,
    width: 400,
    resizable: false,
    modal: true,
    closable: false,
    title: 'Add/Edit applications',
    layout: 'fit',
    updateStores: function () {},
    items: [
        {
            xtype: 'form',
            layout: 'column',
            itemId: 'formPanel',
            defaults: {
                columnWidth: 1,
                allowBlank: false,
                style: {
                    marginBottom: '5px'
                }
            },
            bodyStyle: {
                padding: '5px'
            },
            items: [
                {
                    xtype: 'numberfield',
                    fieldLabel: 'Id (should be hidden; 0 - for new entry)',
                    itemId: 'Id',
                    name: 'Id' // TODO: make it hidden as it's senseless
                },
                {
                    xtype: 'datefield',
                    fieldLabel: 'Date',
                    itemId: 'Date',
                    name: 'Date',
                    format: 'd.m.Y'
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'OrganizationName',
                    itemId: 'OrganizationName',
                    name: 'OrganizationName'
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'UserFullName',
                    itemId: 'UserFullName',
                    name: 'UserFullName'
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Position',
                    itemId: 'Position',
                    name: 'Position'
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Email',
                    itemId: 'Email',
                    name: 'Email'
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Save',
            itemId: 'saveButton',
            handler: function () {}
        },
        {
            text: 'Cancel',
            itemId: 'cancelButton',
            handler: function () {}
        }]
});
