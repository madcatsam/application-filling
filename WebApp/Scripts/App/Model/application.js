﻿Ext.define('App.model.application', {
    extend: 'Ext.data.Model',
    idProperty: 'Id',
    idgen: {
        isGenerator: true,
        type: 'default',
        generate: function () {
            return this;
        },
        getRecId: function (rec) {
            return rec.internalId;
        }
    },
    fields: [
        { name: 'Id', type: 'int' },
        { name: 'Date', type: Ext.data.Types.DATE },
        { name: 'OrganizationName', type: 'string' },
        { name: 'UserFullName', type: 'string' },
        { name: 'Position', type: 'string' },
        { name: 'Email', type: 'string' }
    ]
});
