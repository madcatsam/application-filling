﻿Ext.define('App.model.applicationStore', {
    extend: 'Ext.data.Store',
    model: 'App.model.application',
    pageSize: 10,
    proxy: {
        allowSingle: true,
        type: 'rest',
        url: '/api/applications',
        reader: {
            successProperty: 'success',
            type: 'json',
            totalProperty: 'RecordTotal' // TODO: add on backend
        },
        writer: {
            type: 'json'
        },
        actionMethods:
        {
            read: "GET",
            create: "POST",
            update: "PUT",
            destroy: "DELETE"
        }
    },
    autoLoad: true,
    autoSync: true
});
