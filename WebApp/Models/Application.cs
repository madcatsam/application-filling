﻿using System;

namespace WebApp.Models
{
    /// <summary>
    /// Application model class. Mapping details see EntityConfig.hbm.xml file.
    /// </summary>
    public class Application
    {
        public virtual int Id { get; set; }

        public virtual DateTime Date { get; set; }

        public virtual string OrganizationName { get; set; }

        public virtual string UserFullName { get; set; }

        public virtual string Position { get; set; }

        public virtual string Email { get; set; }
    }
}