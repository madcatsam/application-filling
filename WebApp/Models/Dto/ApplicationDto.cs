﻿using System;

namespace WebApp.Models.Dto
{
    public class ApplicationDto
    {
        public int Id { get; set; } // TODO: Id property needs to be readonly since it's unmodified value

        public DateTime Date { get; set; }

        public string OrganizationName { get; set; }

        public string UserFullName { get; set; }

        public string Position { get; set; }

        public string Email { get; set; }
    }
}