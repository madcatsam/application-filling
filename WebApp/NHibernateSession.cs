﻿using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Configuration;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;
using Configuration = NHibernate.Cfg.Configuration;

namespace WebApp
{
    public static class NHibernateSession
    {
        /// <summary>
        /// Open NHibernate session instance from specified in Web.config connection string param.
        /// </summary>
        /// <returns>Opened NHibernate session instance</returns>
        public static ISession GetSession()
        {
            var connectionString = WebConfigurationManager.AppSettings["ConnectionString"];

            var cfg = new Configuration();
            cfg.DataBaseIntegration(x =>
            {
                x.ConnectionString = connectionString;
                x.Driver<Sql2008ClientDriver>();
                x.Dialect<MsSql2008Dialect>();
            });
            cfg.AddFile(HttpContext.Current.Server.MapPath(@"~\EntityConfig.hbm.xml"));
            cfg.AddAssembly(Assembly.GetExecutingAssembly());
            var sessionFactory = cfg.BuildSessionFactory();

            return sessionFactory.OpenSession();
        }
    }
}